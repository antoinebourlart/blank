#!/usr/bin/env bash

dir="web/media/cache"

echo "Removing liip images"
rm -r $dir

echo "Resolving cache"
for img in assets/images/*.{jpg,png}; do
    name=$(echo $img | cut -d/ -f2-)
    php bin/console liip:imagine:cache:resolve $name
done
