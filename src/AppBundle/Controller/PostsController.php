<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Symfony\Component\HttpFoundation\Request;

class PostsController extends Controller
{
    /**
     * @Route("/posts", name="posts_index")
     * @Template("AppBundle:Posts:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->get('doctrine')->getManager();

        $posts = $em->getRepository('AppBundle:Post')->findAll();

        return [
            "posts" => $posts,
        ];
    }

    /**
     * @Route("/posts/{id}", name="posts_view", requirements={"id": "\d+"})
     * @Template("AppBundle:Posts:view.html.twig")
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->find($i);

        return [
            "post" => $post,
        ];
    }

    /**
     * @Route("/posts/create", name="posts_create")
     * @Template("AppBundle:Posts:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);   
            if ($form->isValid()) {
                $em->persist($post);
                $em->flush();

                return $this->redirectToRoute("posts_view", ["id" => $id]);
            }
        }

        return [
            "post" => $post,
            "form" => $form->createView(),
        ];
    }



}
